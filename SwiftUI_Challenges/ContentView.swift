//
//  ContentView.swift
//  SwiftUI_Challenges
//
//  Created by Thompson,Matthew on 3/10/21.
//

import SwiftUI

extension View {
    /// Superimposes a badge on the view. This will affect the view's layout rect and structure
    func badge(count: Int = 0, in range: ClosedRange<Int>) -> some View {
        ZStack(alignment: .topTrailing) {
            self
            ZStack {
                if count > 0 {
                    let badgeText = count < range.upperBound ? "\(count)" : "\(range.upperBound)+"
                    Text(badgeText)
                        .font(.caption)
                        .padding(12)
                        .background(
                            Circle().fill(Color.red)
                        )
                        .overlay(
                            Circle()
                                .stroke(lineWidth: 1.0)
                                .stroke(Color.black)
                        )
                        .transition(AnyTransition.opacity.combined(with: .scale))
                }
            }
            .alignmentGuide(.trailing) { $0.width / 2 }
            .alignmentGuide(.top) { $0.height / 2 }
        }
    }

    /// Superimposes a badge to the view using an overlay. This does not affect the view's layout rect
    func overlayBadge(count: Int = 0, in range: ClosedRange<Int>) -> some View {
        overlay(
            Group {
                if count > 0 {
                    let badgeText = count < range.upperBound ? "\(count)" : "\(range.upperBound)+"
                    Text(badgeText)
                        .font(.caption)
                        .padding(12)
                        .background(
                            Circle().fill(Color.red)
                        )
                        .overlay(
                            Circle()
                                .stroke(lineWidth: 1.0)
                                .stroke(Color.black)
                        )
                        .transition(AnyTransition.opacity.combined(with: .scale))
                }
            }
            .alignmentGuide(.trailing) { $0.width / 2 }
            .alignmentGuide(.top) { $0.height / 2 }
        , alignment: .topTrailing)
    }
}

struct ContentView: View {
    @State private var counter = 0
    private let range = 0...10

    var body: some View {
        VStack {
            Stepper(value: $counter.animation(.easeInOut(duration: 0.3)), in: range, label: {
                Text("Count")
                    .bold()
            })
            Spacer()
            Text("Hello, World!")
                .padding()
                .background(
                    RoundedRectangle(cornerRadius: 5)
                        .fill(Color.gray))
                .overlayBadge(count: counter, in: 0...(range.upperBound - 1))
            Spacer()
        }.padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
