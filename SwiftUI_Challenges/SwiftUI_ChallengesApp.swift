//
//  SwiftUI_ChallengesApp.swift
//  SwiftUI_Challenges
//
//  Created by Thompson,Matthew on 3/10/21.
//

import SwiftUI

@main
struct SwiftUI_ChallengesApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
